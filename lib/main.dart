import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  double position = 50;
  double left = 0;
  double right = 0;
  bool ignoring = false;
  int redFlex = 1;
  int greenFlex = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Widget Of The Week 5'),
      ),
      body: Center(
        child: Column(
          children: [
            Container(
              child: Stack(
                children: [
                  Container(height: 300, width: 300, color: Colors.red),
                  Positioned(
                    left: position,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(15),
                      child: Container(
                        alignment: Alignment.center,
                        height: 200,
                        width: 200,
                        color: Colors.green,
                        child: Text('Positioned box'),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              width: double.infinity,
              height: 100,
              margin: EdgeInsets.symmetric(vertical: 20, horizontal: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  IgnorePointer(
                    ignoring: position <= 0 ? true : false,
                    child: ElevatedButton(
                      onPressed: () {
                        setState(() {
                          position -= 10;
                        });
                      },
                      child: Text(position <= 0 ? 'Ignore pointer' : '<<<Move left'),
                    ),
                  ),
                  IgnorePointer(
                    ignoring: position >= 100 ? true : false,
                    child: ElevatedButton(
                      onPressed: () {
                        setState(() {
                          position += 10;
                        });
                      },
                      child: Text(position >= 100 ? 'Ignore pointer' : 'Move right>>>'),
                    ),
                  )
                ],
              ),
            ),
            Container(
              height: 100,
              width: double.infinity,
              child: Row(
                children: [
                  Flexible(
                    flex: greenFlex,
                    child: Material(
                      color: Colors.green,
                      child: InkWell(
                        onTap: (){
                          setState(() {
                            greenFlex++;
                          });
                        },
                        child: Container(
                          alignment: Alignment.center,
                          height: 150,
                          child: Text('Tap me to make bigger'),
                        ),
                      ),
                    ),
                  ),
                  Flexible(
                    flex: redFlex,
                    child: Material(
                      color: Colors.red,
                      child: InkWell(
                        onTap: (){
                          setState(() {
                            redFlex++;
                          });
                        },
                        child: Container(
                          alignment: Alignment.center,
                          height: 150,
                          child: Text('Tap me to make bigger'),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
